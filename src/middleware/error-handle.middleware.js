const { CustomAPIError } = require("../errors/customer-error");

const errorHandleMiddleware = (err, req, res, next) => {
  if (err instanceof CustomAPIError) {
    return res.status(err.statusCode).send({ msg: err.message });
  }
  return res.status(500).send({ msg: 'Something went wrong, please try again' });
};
module.exports = errorHandleMiddleware;
