const _task = require("../models/Task");
const asyncWrapper = require("../middleware/async.middleware");
const { createCustomError } = require("../errors/customer-error");

const getAllTasks = asyncWrapper(async (req, res) => {
  const tasks = await _task.find({});
  const total_task = await _task.countDocuments();
  res.status(200).json({ success: true, data: { tasks, nbHits: total_task } });
});

const createTask = asyncWrapper(async (req, res) => {
  const task = await _task.create(req.body);
  res.status(200).json({ task });
});

const getTask = asyncWrapper(async (req, res, next) => {
  const { id: taskID } = req.params;
  const task = await _task.findOne({ _id: taskID });
  if (!task) {
    return next(createCustomError(`No task with id: ${taskID}`, 404));
  }
  res.status(200).json({ task });
});

const updateTask = asyncWrapper(async (req, res) => {
  const { id: taskID } = req.params;
  const task = await _task.findOneAndUpdate({ _id: taskID }, req.body, {
    new: true,
  });
  if (!task) {
    res.status(404).json({ msg: `No task with id: ${taskID}` });
  }
  res.status(200).json({ task });
});

const deleteTask = asyncWrapper(async (req, res) => {
  const { id: taskID } = req.params;
  const task = await _task.findOneAndDelete({ _id: taskID });
  if (!task) {
    res.status(404).json({ msg: `No task with id: ${taskID}` });
  }
  res.status(200).json({ task });
});

module.exports = { getAllTasks, createTask, getTask, updateTask, deleteTask };
