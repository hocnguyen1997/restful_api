require("dotenv").config({ path: `variables.env.development` });
const connectDB = require("./src/db/connect.db");
const tasks = require("./src/routes/task.router");
const notFound = require("./src/middleware/not-found.middleware");
const errorHandleMiddleware = require("./src/middleware/error-handle.middleware");

const express = require("express");
const app = express();

app.use(express.json());

app.get("/hello", (req, res) => {
  res.send("Task manager app");
});

app.use("/api/v1/tasks", tasks);

app.use(notFound);
app.use(errorHandleMiddleware);

const port = process.env.PORT || 5000;
const start = async () => {
  try {
    await connectDB(process.env.MONGO_URL);
    app.listen(port, console.log(`server is listening on port ${port}`));
  } catch (error) {
    console.log(error);
  }
};
start();
